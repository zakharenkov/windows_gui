#ifndef FIND_DIALOG_H
#define FIND_DIALOG_H

extern HWND gFindDialog;
extern int gFoundPosX;
extern int gFoundPosY;

BOOL CALLBACK findDialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam);

#endif
