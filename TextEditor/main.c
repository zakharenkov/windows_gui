#include <windows.h>
#include "resources.h"
#include "FindDialog.h"
#include "TextRendering.h"

const char* gWindowClassName = "Text Editor";
OPENFILENAME gOpenFileName;
HWND gFindDialog;
RECT gWindowRect;
HWND gWindow;
BOOL gWrapMode = FALSE;
HDC	gMemoryDC;
HFONT gFont;

const int MAX_CLIENT_WIDTH  = 1080;
const int MAX_CLIENT_HEIGHT = 1920;

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);


INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, CHAR *CmdLine, INT ShowCmd)
{
	WNDCLASS wc;
	HWND hWnd;
	MSG msg;

	wc.style = CS_VREDRAW | CS_HREDRAW;

	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);

	wc.hInstance = hInstance;
	wc.lpfnWndProc = WindowProcedure;
	wc.lpszClassName = gWindowClassName;
	wc.lpszMenuName = "MAIN_MENU";
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	RegisterClass(&wc);

	hWnd = CreateWindow("Text Editor", gWindowClassName, WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL, 0, 0, 700, 700, NULL, NULL, hInstance, CmdLine);
	ShowWindow(hWnd, SW_SHOWNORMAL);
	UpdateWindow(hWnd);

    while (GetMessage(&msg, NULL, 0, 0) != 0 )
    {
        if (!IsWindow(gFindDialog) || !IsDialogMessage(gFindDialog, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
	return msg.wParam;
}

/** WindowProcedure
DESCRIPTION   processes messages coming from the window
PARAM[IN]     HWND hwnd - handle to the window
PARAM[IN]     UINT message - message
PARAM[IN]     WPARAM wParam - params of message
PARAM[IN]     LPARAM lParam - params of message
RETURN        the interpretation of the return value depends on the particular message
*/
LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
        case WM_CREATE:
        {
            // prepare gOpenFileName structure
            ZeroMemory(&gOpenFileName, sizeof(OPENFILENAME));
            gOpenFileName.lStructSize = sizeof(OPENFILENAME);
            gOpenFileName.hwndOwner = hwnd;
            gOpenFileName.lpstrFile = "\0";
            gOpenFileName.nMaxFile = MAX_PATH;
            gOpenFileName.lpstrFilter = "Text Files(*.txt)\0*.txt\0In Files(*.in)\0*.in\0All Files(*.*)\0*.*\0\0";
            gOpenFileName.nFilterIndex = 1;
            gOpenFileName.lpstrTitle = TEXT("Choose file");
            gOpenFileName.lpstrDefExt = "txt";
            gOpenFileName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

            gWindow = hwnd;

            // Create double buffering DC
            static HBITMAP hBm;
            HDC hdc = GetDC(hwnd);
            gMemoryDC = CreateCompatibleDC(hdc);
            hBm = CreateCompatibleBitmap(hdc, MAX_CLIENT_WIDTH, MAX_CLIENT_HEIGHT);
            SelectObject(gMemoryDC, hBm);
            ReleaseDC(hwnd, hdc);

            // Create and choose font
            gFont = createFont();
            SelectObject(gMemoryDC, gFont);

            //initialize text
            initializeText("Text.h");

            //setup wrap mode
            CheckMenuItem(GetMenu(hwnd), IDM_NOWRAP, MF_CHECKED);
            gWrapMode = FALSE;
        }
        break;
        case WM_COMMAND:
        {
			switch (LOWORD(wParam))
            {
            case IDM_OPEN:
            {
                CHAR filename[MAX_PATH+1] = {0};
                gOpenFileName.lpstrFile = filename;
                if (GetOpenFileName(&gOpenFileName))
                {
                    initializeText(gOpenFileName.lpstrFile);
                }
                SendMessage(hwnd, WM_SIZE, 0, 0L);
                InvalidateRect(hwnd, NULL, TRUE);
            }
            break;
            case IDM_NOWRAP:
            {
                if (gWrapMode == TRUE)
                {
                    HMENU hMenu = GetMenu(hwnd);
                    CheckMenuItem(hMenu, IDM_NOWRAP, MF_CHECKED);
                    CheckMenuItem(hMenu, IDM_WRAP, MF_UNCHECKED);
                    gWrapMode = FALSE;
                    updateCaretPosition(TRUE);
                }
            }
            break;
            case IDM_WRAP:
            {
                if (gWrapMode == FALSE)
                {
                    HMENU hMenu = GetMenu(hwnd);
                    CheckMenuItem(hMenu, IDM_NOWRAP, MF_UNCHECKED);
                    CheckMenuItem(hMenu, IDM_WRAP, MF_CHECKED);
                    gWrapMode = TRUE;
                    updateCaretPosition(TRUE);
                }
            }
            break;
            case IDM_FIND:
            {
                if(!IsWindow(gFindDialog))
                {
                    gFindDialog = CreateDialogParam(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_FIND_DIALOG), hwnd, (DLGPROC) findDialogProc, (LPARAM)NULL);
                    ShowWindow(gFindDialog, SW_SHOW);
                }
            }
            break;
            case IDM_SAVE:
            {
                CHAR filename[MAX_PATH+1] = {0};
                gOpenFileName.lpstrFile = filename;

                if (GetSaveFileName(&gOpenFileName))
                {
                    saveTextToFile(gText, gOpenFileName.lpstrFile);
                }
            }
            break;
            case IDM_EXIT:
            {
                PostQuitMessage(0);
            }
            break;
        }
        }
        case WM_PAINT:
        {
            static PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);

			// clean client area
			static HBRUSH hBrush;
            static HPEN hPen;
            hPen = CreatePen(0, 1, RGB(255, 255, 255));
            hBrush = CreateSolidBrush(RGB(255, 255, 255));
            SelectObject(gMemoryDC, hPen);
            SelectObject(gMemoryDC, hBrush);
            Rectangle(gMemoryDC, 0, 0, MAX_CLIENT_WIDTH, MAX_CLIENT_HEIGHT);
            DeleteObject(hBrush);
            DeleteObject(hPen);

            drawText(&gMemoryDC);

			BitBlt(hdc, 0, 0, MAX_CLIENT_WIDTH, MAX_CLIENT_HEIGHT, gMemoryDC, 0, 0, SRCCOPY);
            EndPaint(hwnd, &ps);
            return 0;
        }
        case WM_SIZE:
        {
            GetClientRect(hwnd, &gWindowRect);
            gVscrollMax = gWindowRect.bottom;
            gHscrollMax = gWindowRect.right;
            SetScrollRange(hwnd, SB_VERT, 0, gVscrollMax, FALSE);
			SetScrollRange(hwnd, SB_HORZ, 0, gHscrollMax, FALSE);
            updateCaretPosition(FALSE);
            InvalidateRect(hwnd, NULL, TRUE);
			return 0;
		}
        case WM_VSCROLL:
		{
		    handleVScrollEvents(wParam);
            break;
		}
        case WM_HSCROLL:
		{
            handleHScrollEvents(wParam);
		    break;
		}
		case WM_SETFOCUS:
		    createCaret();
            updateCaretPosition(TRUE);
			ShowCaret(hwnd);
			break;
		case WM_KILLFOCUS:
			HideCaret(hwnd);
			DestroyCaret();
			break;
		case WM_KEYDOWN:
			switch (wParam)
			{
			/*case VK_HOME:
			    gHscrollPos = 0;
				SetScrollPos(hwnd, SB_HORZ, gHscrollPos, TRUE);
                InvalidateRect(hwnd, NULL, TRUE);
            //SendMessage(hwnd, WM_VSCROLL, SB_TOP, 0L);
				break;
			case VK_END:
				SendMessage(hwnd, WM_VSCROLL, SB_BOTTOM, 0L);
				break;
			case VK_PRIOR:
				SendMessage(hwnd, WM_VSCROLL, SB_PAGEUP, 0L);
				break;
			case VK_NEXT:
				SendMessage(hwnd, WM_VSCROLL, SB_PAGEDOWN, 0L);
				break;*/
			case VK_UP:
				handleKeyUp();
				break;
			case VK_DOWN:
				handleKeyDown();
				break;
			case VK_LEFT:
				handleKeyLeft();
				break;
			case VK_RIGHT:
				handleKeyRight();
				break;
            case VK_DELETE:
                handleKeyDelete();
                break;
            case VK_BACK:
                handleKeyBackspace();
                break;
            case VK_RETURN:
                handleKeyReturn();
                break;
			default:
				handleOtherKey(wParam);
			}
			return 0;
        case WM_DESTROY:
            if(gText)
                freeText(gText);
			DeleteDC(gMemoryDC);
            PostQuitMessage(0);       /* send a WM_QUIT to the message queue */
            break;
        default: /* for messages that we don't deal with */
            return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}
