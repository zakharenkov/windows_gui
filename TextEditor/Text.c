#include "Text.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

/** createTextLine
DESCRIPTION   creates clean instance of text
PARAM[IN]     const char* str - pointer to string with the text
RETURN        pointer to create text structure
*/
TextLine* createTextLine(const char* str)
{
	TextLine* line = (TextLine*)malloc(sizeof(TextLine));
	int len = strlen(str);
	line->capacity = len + 1;
	line->data = (char*)malloc((line->capacity) * sizeof(char));
	strcpy(line->data, str);
	line->size = len;
	return line;
}

/** insertSymbolToLine
DESCRIPTION     inserts a symbol to a TextLine
PARAM[IN/OUT]   TextLine* line - pointer to a TextLine structure
PARAM[IN]       int at - position to insert
PARAM[IN]       char c - symbol to insert
*/
void insertSymbolToLine(TextLine* line, int at, char c)
{
	assert(at <= line->size);

	if (line->size + 1 == line->capacity)
	{
		line->data = realloc(line->data, (line->capacity *= 2)*sizeof(char));
	}

	for (int i = line->size; i >= at; --i)
	{
        line->data[i + 1] = line->data[i];
	}

	line->data[at] = c;
	line->size++;
}

/** appendStringToLine
DESCRIPTION     appends string to TextLine
PARAM[IN/OUT]   TextLine* line - pointer to a TextLine structure
PARAM[IN]       const char* str - string to append
*/
void appendStringToLine(TextLine* line, const char* str)
{
    int appendedLen = strlen(str);

	if (line->size + appendedLen + 1 > line->capacity)
	{
	    int newCapacity = line->size + appendedLen + 1 > line->capacity*2 ? line->size + appendedLen + 1 : line->capacity*2;
		line->data = realloc(line->data, (line->capacity = newCapacity)*sizeof(char));
	}

	strcat(line->data, str);
	line->size += appendedLen;
}

/** insertSymbolToText
DESCRIPTION      inserts a symbol to a Text
PARAM[IN/OUT]    Text* text - pointer to a Text structure
PARAM[IN]        int atX - position in the line to insert to
PARAM[IN]        int atY - number of the line to insert to
PARAM[IN]        char c - symbol to insert
*/
void insertSymbolToText(Text* text, int atX, int atY, char c)
{
	if (!text)
		return;

	assert(atY < text->size);
	assert(atX <= text->lines[atY]->size);

  	insertSymbolToLine(text->lines[atY], atX, c);

	if (text->lines[atY]->size > text->maxLineLength)
		text->maxLineLength = text->lines[atY]->size;
}

/** deleteSymbolFromLine
DESCRIPTION     deletes a symbol from a TextLine
PARAM[IN/OUT]   TextLine* line - pointer to a TextLine structure
PARAM[IN]       int at - position in the line to delete from
*/
void deleteSymbolFromLine(TextLine* line, int at)
{
	assert(at < line->size);

	for (int i = at; i < line->size; ++i)
		line->data[i] = line->data[i + 1];

	line->size--;
}

/** freeTextLine
DESCRIPTION   frees memory from a TextLine structure
PARAM[IN]     TextLine* line - pointer to a TextLine structure
*/
void freeTextLine(TextLine* line)
{
    if (line)
    {
        free(line->data);
        free(line);
    }
}

/** createText
DESCRIPTION   creates a Text instance from a string
RETURN        a pointer to newly created Text
*/
Text* createText()
{
	Text* text = (Text*)malloc(sizeof(Text));
	text->capacity = 1;
	text->lines = (TextLine**)malloc(1 * sizeof(TextLine*)); // create buffer for 1 line
	text->size = 0;
	text->maxLineLength = 0;
    return text;
}

/** insertLineToText
DESCRIPTION     creates a Text instance from a string
PARAM[IN/OUT]   Text* text - text to insert line to
PARAM[IN]       int at - position to insert to
PARAM[IN]       const char* str - string to make a line from
*/
void insertLineToText(Text* text, int at, const char* str)
{
	assert(at <= text->size);

	if (text->size == text->capacity)
	{
		text->lines = realloc(text->lines, (text->capacity *= 2) * sizeof(TextLine*));
	}

	for (int i = text->size - 1; i >= at; --i)
		text->lines[i + 1] = text->lines[i];

	text->lines[at] = createTextLine(str);
	text->size++;
	int newLineLength = strlen(str);
	if (newLineLength > text->maxLineLength)
		text->maxLineLength = newLineLength;
}

/** updateMaxLineLength
DESCRIPTION     calculates and updates maximum line length in a Text instance
PARAM[IN/OUT]   Text* text - text instance
RETURN          int - maximum line length
*/
int updateMaxLineLength(Text* text)
{
	if (!text)
			return 0;

	int maxLen = 0;
	for (int i = 0; i < text->size; ++i)
	{
		int curLen = text->lines[i]->size;
		maxLen = curLen > maxLen ? curLen : maxLen;
	}
	return text->maxLineLength = maxLen;
}

/** splitTextLine
DESCRIPTION     splits the Text line by making two lines
PARAM[IN/OUT]   Text* text
PARAM[IN]       int atX - position in the line where to split
PARAM[IN]       int atY - number of the line to split
*/
void splitTextLine(Text* text, int atX, int atY)
{
    assert(atY < text->size);
    assert(atX <= text->lines[atY]->size);
    insertLineToText(text, atY+1, text->lines[atY]->data + atX);
    text->lines[atY]->data[atX] = '\0';
    text->lines[atY]->size = atX;
    updateMaxLineLength(text);
}

/** mergeTextLines
DESCRIPTION     merges two neighbouring lines of Text to one
PARAM[IN/OUT]   Text* text
PARAM[IN]       int Y - lines number Y and (Y+1) will be merged
*/
void mergeTextLines(Text* text, int Y)
{
    assert(Y >= 0 && Y < text->size);
    if (Y == text->size - 1)
        return;

    appendStringToLine(text->lines[Y], text->lines[Y+1]->data);
    removeLineFromText(text, Y+1);

    if (text->lines[Y]->size > text->maxLineLength)
        text->maxLineLength = text->lines[Y]->size;
}

/** deleteSymbolFromText
DESCRIPTION     deletes a symbol from a Text instance
PARAM[IN/OUT]   Text* text - text to delete a symbol from
PARAM[IN]       int atX - position of a symbol in a line
PARAM[IN]       int atY - number of line
*/
void deleteSymbolFromText(Text* text, int atX, int atY)
{
	assert(atY < text->size);

	if (atX < text->lines[atY]->size)
    {
        deleteSymbolFromLine(text->lines[atY], atX);
        updateMaxLineLength(text);
    }
    else
    {
        if (atY < text->size - 1)
        {
            mergeTextLines(text, atY);
        }
    }
}

/** removeLineFromText
DESCRIPTION     removes line from a Text
PARAM[IN/OUT]   Text* text - text to delete line from
PARAM[IN]       int at - number of the line to remove
*/
void removeLineFromText(Text* text, int at)
{
	assert(at < text->size);

	freeTextLine(text->lines[at]);

	for (int i = at; i < text->size - 1; ++i)
		text->lines[i] = text->lines[i + 1];

	text->size--;
	updateMaxLineLength(text);

	if (text->size == 0)
        insertLineToText(text, 0, "");
}

/** freeText
DESCRIPTION     frees memory from a Text structure
PARAM[IN/OUT]   Text* text - text free
*/
void freeText(Text* text)
{
    if (text)
    {
        for (int i = 0; i < text->size; ++i)
            freeTextLine(text->lines[i]);
        free(text->lines);
        free(text);
    }
}

/** createTextFromFile
DESCRIPTION     creates a Text instance with data from a file
PARAM[IN]       const char* filename - file name to take the text from
RETURN          a pointer to created Text instance
*/
Text* createTextFromFile(const char* filename)
{
	Text* text = createText(NULL);

	FILE* pf = fopen(filename, "rt");
	if (pf)
    {
        char* buf = (char*)malloc(100000 * sizeof(char));

        while (1)
        {
            buf[0] = '\0';
            fgets(buf, 100000, pf);
            int len = strlen(buf);
            if (len == 0 && feof(pf))
                break;

            while(len > 0 && (buf[len-1] == '\n' || buf[len-1] == '\r'))
            {
                buf[len-1] = '\0';
                len--;
            }

            insertLineToText(text, text->size, buf);
        }

        fclose(pf);
        free(buf);
    }

    if (text->size == 0)
        insertLineToText(text, 0, "");
	return text;
}

/** saveTextToFile
DESCRIPTION     saves the Text contents to a file
PARAM[IN]       const Text* text - the Text to save
PARAM[IN]       const char* filename - file name to save the Text
*/
void saveTextToFile(const Text* text, const char* filename)
{
    FILE* pf = fopen(filename, "w");
    if (pf)
    {
        if (text)
        {
            for (int i = 0; i < text->size; ++i)
            {
                fputs(text->lines[i]->data, pf);
                fputc('\n', pf);
            }
        }
        fclose(pf);
    }
}
