#include <Windows.h>
#include "globals.h"
#include "FindDialog.h"
#include "TextRendering.h"
#include <assert.h>
#include <ctype.h>

Text* gText = NULL;

int gCaretX = 0; // caret position in the window in symbols, counting from left upper corner
int gCaretY = 0;

int gWrappedLinesCount = 0; // number of lines in text with account for wrapping
int gTopVisibleWrappedLine = 0; // the number of topmost visible line with account for wraps
int gFirstVisibleLetter = 0; // the number of leftmost visible letter
int gTextX = 0; // the number of current letter in current text line
int gTextY = 0; // the number of current text line


int gVscrollPos, gHscrollPos;
int gVscrollMax, gHscrollMax;

const int LETTER_HEIGHT = 16;
const int LETTER_WIDTH = 10;

/** createFont
DESCRIPTION   creates font for the main window
*/
HFONT createFont()
{
    return CreateFont(LETTER_HEIGHT + 2,
                8,  // symbol width
                0,
                0,
                FW_BOLD,
                0,       // italic
                0,       // underline
                0,       // strikeout
                DEFAULT_CHARSET,    // character set identifier
                OUT_DEVICE_PRECIS,  // output precision
                CLIP_DEFAULT_PRECIS,// clipping precision
                DEFAULT_QUALITY,    // output quality
                DEFAULT_PITCH,      // pitch and family
                "Courier");
}

/** createCaret
DESCRIPTION   creates caret for the main window
*/
void createCaret()
{
    CreateCaret(gWindow, (HBITMAP)0, 3, LETTER_HEIGHT);
}

/** drawText
DESCRIPTION     draw visible piece of text in the device context
PARAM[IN/OUT]   HDC* hdc - device context to draw in
*/
void drawText(HDC* hdc)
{
    if(gText)
    {
        int linesInPage = gVscrollMax / LETTER_HEIGHT;
        int lineAfterLast = min(gWrappedLinesCount, gTopVisibleWrappedLine + linesInPage);

        if (!gWrapMode)
        {
            for (int i = gTopVisibleWrappedLine; i < lineAfterLast; ++i)
            {
                int Y = LETTER_HEIGHT * (i - gTopVisibleWrappedLine);
                int X = -LETTER_WIDTH * gFirstVisibleLetter;

                if (i == gFoundPosY)
                {
                    assert(gFoundPosX < gText->lines[i]->size);

                    TextOut(*hdc, X, Y, gText->lines[i]->data, gFoundPosX);
                    SetBkColor(*hdc, 0x00FF00);
                    TextOut(*hdc, X + gFoundPosX*LETTER_WIDTH, Y, gText->lines[i]->data + gFoundPosX, 1);
                    SetBkColor(*hdc, 0xFFFFFF);
                    TextOut(*hdc, X + (gFoundPosX+1)*LETTER_WIDTH, Y, gText->lines[i]->data + gFoundPosX + 1,
                            gText->lines[i]->size - gFoundPosX - 1);
                }
                else
                {
                    TextOut(*hdc, X, Y, gText->lines[i]->data, gText->lines[i]->size);
                }
            }
        }
        else
        {
            int realLine = 0;
            int windowWidth = gWindowRect.right / LETTER_WIDTH;

            for (int i = 0; i < gText->size; ++i)
            {
                int curLen = gText->lines[i]->size;
                int curCnt = (curLen == 0 ? 1 : (curLen + windowWidth - 1) / windowWidth);

                int X = -LETTER_WIDTH * gFirstVisibleLetter;

                for (int j = 0; j < curCnt; ++j, ++realLine)
                {
                    if (realLine < gTopVisibleWrappedLine)
                        continue;
                    if (realLine >= lineAfterLast)
                        return;
                    int Y = LETTER_HEIGHT * (realLine - gTopVisibleWrappedLine);

                    if (i == gFoundPosY && j*windowWidth <= gFoundPosX && gFoundPosX < (j+1)*windowWidth)
                    {
                        TextOut(*hdc, X, Y,
                                gText->lines[i]->data + j*windowWidth, gFoundPosX - j*windowWidth);
                        SetBkColor(*hdc, 0x00FF00);
                        TextOut(*hdc, X + (gFoundPosX - j*windowWidth)*LETTER_WIDTH, Y,
                                gText->lines[i]->data + gFoundPosX, 1);
                        SetBkColor(*hdc, 0xFFFFFF);
                        TextOut(*hdc, X + (gFoundPosX - j*windowWidth + 1)*LETTER_WIDTH, Y,
                                gText->lines[i]->data + gFoundPosX+1, gText->lines[i]->size - gFoundPosX-1);
                    }
                    else
                    {
                        int len = (j < curCnt - 1 ? windowWidth : gText->lines[i]->size - j*windowWidth);

                        TextOut(*hdc, X, Y,
                                gText->lines[i]->data + j*windowWidth, len);
                    }
                }

            }
        }
    }
}

/** updateVScroll
DESCRIPTION     recalculates vertical scroll position, sets vertical scrollbar and provokes redraw
*/
void updateVScroll()
{
    gVscrollPos = gWrappedLinesCount > 1
                      ? (double)gTopVisibleWrappedLine/(gWrappedLinesCount-1) * gVscrollMax
                      : 0;
    SetScrollPos(gWindow, SB_VERT, gVscrollPos, TRUE);
    InvalidateRect(gWindow, NULL, TRUE);
}

/** initializeText
DESCRIPTION     recreates a Text instance and resets drawing parameters
PARAM[IN]       const char* filename - file name with a new text
*/
void initializeText(const char* filename)
{
    freeText(gText);
    gText = NULL;
    gText = createTextFromFile(filename);
    gVscrollPos = gHscrollPos = 0;
    gTopVisibleWrappedLine = gFirstVisibleLetter = 0;
    gTextX = gTextY = 0;
    gCaretX = gCaretY = 0;
    updateVScroll();
}

/** calculatePositionInText
DESCRIPTION     calculates position in a Text by position in a window and windowWidth
PARAM[IN]       int realX - x-position of a symbol if wraps taken into account
PARAM[IN]       int realY - y-position of a symbol if wraps taken into account
PARAM[IN]       windowWidth - window width in symbols (-1 if no wrapping)
PARAM[OUT]      int* textX - symbol number in a text line
PARAM[OUT]      int* textY - the number of a text line
*/
void calculatePositionInText(int realX, int realY, int windowWidth, int* textX, int* textY)
{
	if (!gText)
	{
		*textX = *textY = 0;
	}
	else if (windowWidth == -1)
	{
		*textY = max(0, min(realY, gText->size - 1));
		*textX = min(realX, gText->lines[*textY]->size);
	}
	else
	{
	    realY = max(0, realY);
		int realLinesCount = 0;

		for (int line = 0; line < gText->size; ++line)
		{
			int curLen = gText->lines[line]->size;
			int curCnt = (curLen == 0 ? 1 : (curLen + windowWidth - 1) / windowWidth);

            for (int j = 0; j < curCnt; ++j, ++realLinesCount)
            {
                if (realY == realLinesCount)
                {
                    *textY = line;
                    *textX = min(j*windowWidth + realX, curLen);
                    return;
                }
            }
		}

		*textY = max(0, gText->size - 1);
		*textX = min(*textX, (gText->size ? gText->lines[gText->size-1]->size : 0));
	}
}

/** handleKeyRight
DESCRIPTION     handles arrow 'right' press
*/
void handleKeyRight()
{
    if (gTextX < gText->lines[gTextY]->size)
    {
        gTextX++;
    }
    else if (gTextY < gText->size - 1)
    {
        gTextX = 0;
        gTextY++;
    }
    updateCaretPosition(FALSE);
}

/** handleKeyLeft
DESCRIPTION     handles arrow 'left' press
*/
void handleKeyLeft()
{
    if (gTextX > 0)
    {
        gTextX--;
    }
    else if (gTextY > 0)
    {
        gTextY--;
        gTextX = gText->lines[gTextY]->size;
    }
    updateCaretPosition(FALSE);
}

/** handleKeyUp
DESCRIPTION     handles arrow 'up' press
*/
void handleKeyUp()
{
    calculatePositionInText(gCaretX + gFirstVisibleLetter,
                            gCaretY + gTopVisibleWrappedLine - 1,
                            (gWrapMode ? gWindowRect.right / LETTER_WIDTH : -1),
                            &gTextX, &gTextY);

    updateCaretPosition(FALSE);
}

/** handleKeyDown
DESCRIPTION     handles arrow 'down' press
*/
void handleKeyDown()
{
    calculatePositionInText(gCaretX + gFirstVisibleLetter,
                            gCaretY + gTopVisibleWrappedLine + 1,
                            (gWrapMode ? gWindowRect.right / LETTER_WIDTH : -1),
                            &gTextX, &gTextY);

    updateCaretPosition(FALSE);
}

/** handleKeyDelete
DESCRIPTION     handles key 'delete' press
*/
void handleKeyDelete()
{
    deleteSymbolFromText(gText, gTextX, gTextY);
    updateCaretPosition(FALSE);
    InvalidateRect(gWindow, NULL, TRUE);
}

/** handleKeyBackspace
DESCRIPTION     handles key 'backspace' press
*/
void handleKeyBackspace()
{
    if (gTextX > 0)
    {
        gTextX--;
        deleteSymbolFromText(gText, gTextX, gTextY);
        updateCaretPosition(FALSE);
        InvalidateRect(gWindow, NULL, TRUE);
    }
    else if (gTextY > 0)
    {
        gTextX = gText->lines[gTextY-1]->size;
        gTextY--;
        mergeTextLines(gText, gTextY);
        updateCaretPosition(FALSE);
        InvalidateRect(gWindow, NULL, TRUE);
    }
}

/** handleKeyReturn
DESCRIPTION     handles key 'return' press
*/
void handleKeyReturn()
{
    splitTextLine(gText, gTextX, gTextY);
    gTextY++;
    gTextX = 0;
    updateCaretPosition(FALSE);
    InvalidateRect(gWindow, NULL, TRUE);
}

/** handleOtherKey
DESCRIPTION     handles printable keys press
PARAM[IN]       WPARAM virtual_key
*/
void handleOtherKey(WPARAM virtual_key)
{
    if ( (virtual_key >= 0x30 && virtual_key <= 0x39) ||
         (virtual_key >= 0x41 && virtual_key <= 0x5A) ||
         virtual_key == VK_SPACE)
    {

        char c = tolower(virtual_key);

        insertSymbolToText(gText, gTextX, gTextY, c);
        gTextX++;

        updateCaretPosition(FALSE);
        InvalidateRect(gWindow, NULL, TRUE);
    }
}

/** returnCaretToWindow
DESCRIPTION     corrects caret position to fit into window client area
RETURN          whether caret was really moved and redrawing is necessary
*/
BOOL returnCaretToWindow()
{
    int linesInWindow = gWindowRect.bottom / LETTER_HEIGHT;
    if (linesInWindow == 0)
        return FALSE;

    BOOL needRedraw = FALSE;

    if (gCaretY >= linesInWindow)
    {
        gTopVisibleWrappedLine += (gCaretY - linesInWindow + 1);
        gCaretY = linesInWindow - 1;
        needRedraw = TRUE;
    }
    else if (gCaretY < 0)
    {
        gTopVisibleWrappedLine += gCaretY;
        gCaretY = 0;
        needRedraw = TRUE;
    }

    return needRedraw;
}

/** calculateRealPositionAndLinesCount
DESCRIPTION     calculates position in text and counts real lines with account for wrapping
PARAM[IN]       int textX - symbol number in a text line
PARAM[IN]       int textY - the number of a text line
PARAM[IN]       windowWidth - window width in symbols (-1 if no wrapping)
PARAM[OUT]      int* realX - x-position of a symbol if wraps taken into account
PARAM[OUT]      int* realY - y-position of a symbol if wraps taken into account
PARAM[OUT]      int* realLinesCount - lines count int the text with account for wrapping
*/
void calculateRealPositionAndLinesCount(int textX, int textY, int windowWidth,
                                        int* realX, int* realY, int* realLinesCount)
{
	if (!gText)
	{
		*realX = *realY = *realLinesCount = 0;
	}
	else if (windowWidth == -1)
	{
		*realX = textX;
		*realY = textY;
		*realLinesCount = gText->size;
	}
	else if (textY >= gText->size)
	{
		*realX = gText->lines[gText->size-1]->size;
		*realY = (gText->size ? gText->size - 1 : 0);
		return;
	}
	else
	{
	    *realLinesCount = 0;
	    *realX = -1;
	    *realY = -1;

		for (int line = 0; line < gText->size; ++line)
		{
			int curLen = gText->lines[line]->size;

			if (line == textY) // then symbol is in this line (or beyond its length)
			{
			    assert(textX <= curLen);
				int localLineNumber = textX / windowWidth;
				int localX = textX % windowWidth;

				*realX = localX;
				*realY = *realLinesCount + localLineNumber;
			}

			*realLinesCount += (curLen == 0 ? 1 : (curLen + windowWidth - 1) / windowWidth);
		}
	}
}

/** updateCaretPosition
DESCRIPTION     corrects caret position to fit into window client area
PARAM[IN]       BOOL afterWrapModeChange - if the function invocation is
                the result of changing wrap mode then some other actions must be performed
*/
void updateCaretPosition(BOOL afterWrapModeChange)
{
    int windowWidth = gWindowRect.right / LETTER_WIDTH;

    int realX, realY;
    calculateRealPositionAndLinesCount(gTextX, gTextY, gWrapMode ? windowWidth : -1,
                                        &realX, &realY, &gWrappedLinesCount);

    gCaretX = realX - gFirstVisibleLetter;

    if (!afterWrapModeChange)
    {
        gCaretY = realY - gTopVisibleWrappedLine;
    }
    else
    {
        gTopVisibleWrappedLine = realY;
        gCaretY = 0;
    }

    BOOL needRedraw = returnCaretToWindow() || afterWrapModeChange;
    SetCaretPos(gCaretX * LETTER_WIDTH, gCaretY * LETTER_HEIGHT);

    if (needRedraw)
    {
        updateVScroll();
    }
}

/** handleVScrollEvents
DESCRIPTION     handles vertical scrolling events
PARAM[IN]       WPARAM scrollMessage
*/
void handleVScrollEvents(WPARAM scrollMessage)
{
    /* scroll variables are in this relation:
       when gTopVisibleWrappedLine is gWrappedLinesCount then gVscrollPos is gVscrollMax.
       So:
        gTopVisibleWrappedLine   gVscrollPos
       ----------------------- = -----------
        gWrappedLinesCount       gVscrollMax
    */

    int oldTopVisibleLine = gTopVisibleWrappedLine;

    switch (LOWORD(scrollMessage))
    {
    case SB_LINEUP:
        if (gTopVisibleWrappedLine > 0)
        {
            gTopVisibleWrappedLine--;
        }
        break;
    case SB_LINEDOWN:
        if (gTopVisibleWrappedLine < gWrappedLinesCount - 1)
        {
            gTopVisibleWrappedLine++;
        }
        break;
    case SB_PAGEUP:
        if (gTopVisibleWrappedLine > 0)
        {
            int linesInPage = gVscrollMax / LETTER_HEIGHT;
            gTopVisibleWrappedLine = max(0, gTopVisibleWrappedLine - linesInPage);
        }
        break;
    case SB_PAGEDOWN:
        if (gTopVisibleWrappedLine < gWrappedLinesCount - 1)
        {
            int linesInPage = gVscrollMax / LETTER_HEIGHT;
            gTopVisibleWrappedLine = min(gWrappedLinesCount - 1, gTopVisibleWrappedLine + linesInPage);
        }
        break;
    case SB_THUMBTRACK:
        {
            gVscrollPos = HIWORD(scrollMessage);
            gTopVisibleWrappedLine = (double)gVscrollPos/gVscrollMax * gWrappedLinesCount;
            gTopVisibleWrappedLine = min(gTopVisibleWrappedLine, gWrappedLinesCount-1);
        }
        break;
    }

    if (oldTopVisibleLine != gTopVisibleWrappedLine)
    {
        updateVScroll();

        calculatePositionInText(gCaretX + gFirstVisibleLetter,
                                gCaretY + gTopVisibleWrappedLine,
                                (gWrapMode ? gWindowRect.right / LETTER_WIDTH : -1),
                                &gTextX, &gTextY);

        updateCaretPosition(FALSE);
    }
}

/** handleHScrollEvents
DESCRIPTION     handles horizontal scrolling events
PARAM[IN]       WPARAM scrollMessage
*/
void handleHScrollEvents(WPARAM scrollMessage)
{
    /* scroll variables are in this relation:
       when gFirstVisibleLetter is gText->maxLineLength then gHscrollPos is gHscrollMax.
       So:
        gFirstVisibleLetter      gHscrollPos
       ----------------------- = -----------
        gText->maxLineLength     gHscrollMax
    */

    int oldFirstVisibleLetter = gFirstVisibleLetter;

    switch (LOWORD(scrollMessage))
    {
    case SB_LINELEFT:
        if (gFirstVisibleLetter > 0)
        {
            gFirstVisibleLetter--;
        }
        break;
    case SB_LINERIGHT:
        if (gFirstVisibleLetter < gText->maxLineLength - 1)
        {
            gFirstVisibleLetter++;
        }
        break;
    case SB_PAGELEFT:
        if (gFirstVisibleLetter > 0)
        {
            int lettersInLine = gHscrollMax / LETTER_WIDTH;
            gFirstVisibleLetter = max(0, gFirstVisibleLetter - lettersInLine);
        }
        break;
    case SB_PAGERIGHT:
        if (gFirstVisibleLetter < gText->maxLineLength - 1)
        {
            int lettersInLine = gHscrollMax / LETTER_WIDTH;
            gFirstVisibleLetter = min(gText->maxLineLength - 1, gFirstVisibleLetter + lettersInLine);
        }
        break;
    case SB_THUMBTRACK:
        {
            gHscrollPos = HIWORD(scrollMessage);
            gFirstVisibleLetter = (double)gHscrollPos/gHscrollMax * gText->maxLineLength;
            gFirstVisibleLetter = min(gFirstVisibleLetter, gText->maxLineLength-1);
        }
        break;
    }

    if (oldFirstVisibleLetter != gFirstVisibleLetter)
    {
        updateVScroll();

        gVscrollPos = gText->maxLineLength > 1
                      ? (double)gFirstVisibleLetter/(gText->maxLineLength-1) * gHscrollMax
                      : 0;

        updateCaretPosition(FALSE);
    }
}
