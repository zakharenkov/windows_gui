#include <windows.h>
#include <string.h>
#include <assert.h>
#include "FindDialog.h"
#include "resources.h"
#include "globals.h"
#include "TextRendering.h"

HWND gFindDialog = NULL;

int gFoundPosX = -1;
int gFoundPosY = -1;

/** findDialogProc
DESCRIPTION   processes messages coming from find dialog
PARAM[IN]     HWND hwndDlg - handle to dialog window
PARAM[IN]     UINT message - message
PARAM[IN]     WPARAM wParam - params of message
PARAM[IN]     LPARAM lParam - params of message
RETURN        TRUE if the message is processed by the procedure
*/
BOOL CALLBACK findDialogProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    static int searchPosX, searchPosY;

    switch (message)
    {
        case WM_INITDIALOG:
        {
            searchPosX = 0;
            searchPosY = 0;
            gFoundPosX = -1;
            gFoundPosY = -1;
            return TRUE;
        }
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
            case ID_FIND_NEXT:
            {
                int searchTextLen = GetWindowTextLength(GetDlgItem(gFindDialog, ID_EDIT));

                if (searchTextLen == 0)
                    return TRUE;

                char* searchText = (char*)calloc(searchTextLen + 1, sizeof(char));
                if (searchText != NULL)
                {
                    GetDlgItemText(gFindDialog, ID_EDIT, searchText, searchTextLen + 1);

                    while (searchPosY < gText->size)
                    {
                        char* lineData = gText->lines[searchPosY]->data;


                        assert(searchPosX <= gText->lines[searchPosY]->size);
                        char* foundPos = strstr(lineData + searchPosX, searchText);

                        if (foundPos)
                        {
                            searchPosX = foundPos - lineData;
                            gFoundPosX = searchPosX;
                            gFoundPosY = searchPosY;

                            gTextX = gFoundPosX;
                            gTextY = gFoundPosY;
                            updateCaretPosition(FALSE);
                            InvalidateRect(gWindow, NULL, TRUE);

                            searchPosX++;
                            break;
                        }
                        else
                        {
                            searchPosX = 0;
                            searchPosY++;
                        }

                    }

                    if (searchPosY >= gText->size)
                    {
                        searchPosX = 0;
                        searchPosY = 0;
                        gFoundPosX = -1;
                        gFoundPosY = -1;
                        InvalidateRect(gWindow, NULL, TRUE);
                    }
                }

                free(searchText);
            }
            return TRUE;

            case ID_FIND_CANCEL:
            case IDCANCEL:
                DestroyWindow(hwndDlg);
                gFindDialog = NULL;
                gFoundPosX = -1;
                gFoundPosY = -1;
                return TRUE;
            }
    }
    return FALSE;
}
