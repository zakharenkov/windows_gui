#ifndef RESOURSES_H
#define RESOURSES_H

#define IDM_OPEN   10001
#define IDM_EXIT   10002
#define IDM_NOWRAP 10003
#define IDM_WRAP   10004
#define IDM_FIND   10005
#define IDM_SAVE   10006

#define IDD_FIND_DIALOG   10020
#define ID_EDIT		      10021
#define ID_FIND_NEXT      10022
#define ID_FIND_CANCEL    10023
#define ID_STATUSBAR      10024

#endif
