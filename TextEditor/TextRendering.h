#ifndef TEXT_RENDERING_H
#define TEXT_RENDERING_H

#include "Text.h"

extern Text* gText;
extern int gTextX, gTextY;

extern int gVscrollPos, gHscrollPos;
extern int gVscrollMax, gHscrollMax;

HFONT createFont();
void createCaret();
void drawText(HDC *hMemDC);
void initializeText(const char* fileName);

void handleKeyLeft();
void handleKeyRight();
void handleKeyUp();
void handleKeyDown();
void handleKeyDelete();
void handleKeyBackspace();
void handleKeyReturn();
void handleOtherKey(WPARAM virtual_key);

void handleVScrollEvents(WPARAM scrollMessage);
void handleHScrollEvents(WPARAM scrollMessage);

void updateCaretPosition(BOOL afterWrapModeChange);

#endif // TEXT_RENDERING_H
