Created using Code::Blocks v. 16.01 and gnu-gcc v. 4.9.2

This program is a texteditor written in C using WinAPI. It resembles Windows' Notepad without options but having subsring search and, unlike Notepad, it is capable of processing multi-Mb files without hanging.