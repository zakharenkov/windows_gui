#ifndef TEXT_H
#define TEXT_H

/** TextLine
DESCRIPTION  structure for internal use to store one line of text,
             i.e. a piece of text bounded with line feeds or file begin/end
*/
typedef struct _TextLine
{
    char* data; // buffer with symbols
    int size; // symbols used (not including null-symbol)
    int capacity; // buffer size
} TextLine;


/** Text
DESCRIPTION  a dynamic data structures representing text
*/
typedef struct _Text
{
    TextLine** lines;
    int size;
    int capacity;
    int maxLineLength;
} Text;

Text* createText();
Text* createTextFromFile(const char* filename);
void saveTextToFile(const Text* text, const char* filename);
void insertLineToText(Text* text, int at, const char* str);
void removeLineFromText(Text* text, int at);
void mergeTextLines(Text* text, int Y);
void splitTextLine(Text* gText, int atX, int atY);
void insertSymbolToText(Text* text, int atX, int atY, char c);
void deleteSymbolFromText(Text* text, int atX, int atY);
void freeText(Text* text);

#endif // TEXT_H
